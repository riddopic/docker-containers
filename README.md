# Building Docker Images

This repository comes with a `Rakefile` that is used to build the Docker
containers I use in Chef Cookbook development. There are two commands:

  * `docker:build`
  * `docker:build_all`

## `docker:build`
Use the `build` command when you want to build a single image locally. The
`build` command accepts one parameters: `image_name`.

  * `image_name` is the name of the `container` image you wish to build. It
    must correspond with a folder inside the `Dockerfiles` folder. If no value
	is specified, it will default to `centos-6`.

## `docker:build_all`
The `build_all` command will build each image that is available in the
Dockerfiles directory.


rake docker:build[centos-6]
rake docker:build[centos-7]
rake docker:build[ubuntu-14.04]
rake docker:build[chef-server]

rake docker:build[alpine]
rake docker:build[consul]
rake docker:build[consul-ui]

rake docker:build[supervisord]
rake docker:build[serf]
rake docker:build[nginx]
rake docker:build[redis]

# Clean em
docker rm `docker ps -a | grep Exited | awk '{ print $1 }'`
docker rmi -f `docker images -aq`
