# # encoding: UTF-8

require 'rake'
require 'erb'
require 'tmpdir'
require 'hoodie'

# Stick a date on it, call it the version number
@latest_version = Time.now.strftime('%y.%-m.%d')

namespace :docker do
  desc 'build specified image'
  task :build, :image_name, :version do |t, args|
    image_name    = args[:image_name] || 'centos-6'
    version       = args[:version]    || @latest_version
    is_latest     = version           == @latest_version

    version_tags  = Array(version)
    version_tags << version.split('.')[0,2].join('.')
    version_tags << version.split('.')[0,1].join('.')
    version_tags << 'latest' if is_latest

    puts "Rendering Dockerfile for #{image_name}:#{version}.".yellow
    render_dockerfile(image_name, version)

    Dir[File.join(File.expand_path(File.dirname(__FILE__)),
      'Dockerfiles', image_name, '*')
    ].delete_if {|i| i =~ /Dockerfile.erb/}.map {|f| FileUtils.cp_r(f, tempdir)}

    puts 'Dockerfile rendered. Building Docker image...'.yellow
    output = %x{docker build #{tempdir}}
    puts output
    image_id = output.match(/^Successfully built ([a-zA-Z0-9]+)$/)[1]

    # Verify to make sure chef-init is installed correctly
    puts "Image #{image_id} build complete. Verifying build..."
    pass = %w{docker run --rm #{image_id} chef-init --verify}
    unless pass
      puts 'FATAL: Verification failed!'.red
      exit
    else
      puts 'Build passed.'.green
    end

    # Tag the image with the various version tags
    puts "Tagging #{image_id} with version tags:"
    version_tags.each do |tag|
      puts "\triddopic/#{image_name}:#{tag}".green
      %x{docker tag -f #{image_id} riddopic/#{image_name}:#{tag}}
    end

    clear_tempdir
  end

  desc 'build all the available images'
  task :build_all, :version do |t, args|
    version = args[:version] || @latest_version
    platforms = %w(centos-6 centos-7 supervisord-6 supervisord-7
                   serf-6 serf-7 dnsmasq-6 dnsmasq-7)
    # Dir.entries('Dockerfiles').select do |platform|
    platforms.each do |platform|
      unless platform == '.' || platform == '..'
        Rake::Task['docker:build'].invoke(platform, version)
        Rake::Task['docker:build'].reenable
      end
    end
  end
end

def render_dockerfile(image_name, version)
  dockerfile = File.join(File.expand_path(File.dirname(__FILE__)),
    'Dockerfiles', image_name, 'Dockerfile.erb'
  )
  template = ERB.new(File.read(dockerfile))
  result   = template.result(binding)
  dest     = "#{tempdir}/Dockerfile"

  File.open(dest, 'w') { |file| file.write(result) }
end

def tempdir
  @tmpdir ||= Dir.mktmpdir('dockerfile')
  File.realpath(@tmpdir)
end

def clear_tempdir
  FileUtils.rm_rf(tempdir)
  @tmpdir = nil
end
